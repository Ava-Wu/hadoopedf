package org.yyw.HadoopEDF;

import java.io.DataOutputStream;
import java.io.IOException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.compress.CompressionCodec;
import org.apache.hadoop.io.compress.GzipCodec;
import org.apache.hadoop.mapreduce.RecordWriter;
import org.apache.hadoop.mapreduce.TaskAttemptContext;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.ReflectionUtils;

public class OverwriteTextOutputFormat<K,V> extends TextOutputFormat<K,V> {
	
       public RecordWriter<K,V>
       getRecordWriter(TaskAttemptContext job) throws IOException, InterruptedException 
       {
           Configuration conf = job.getConfiguration();
           boolean isCompressed = getCompressOutput(job);
           String keyValueSeparator= conf.get("mapred.textoutputformat.separator",":");
           CompressionCodec codec = null;
           String extension = "";
           if (isCompressed) 
           {
               Class<? extends CompressionCodec> codecClass = 
                       getOutputCompressorClass(job, (Class<? extends CompressionCodec>) GzipCodec.class);
               codec = (CompressionCodec) ReflectionUtils.newInstance(codecClass, conf);
               extension = codec.getDefaultExtension();
           }
           Path file = getDefaultWorkFile(job, extension);
           FileSystem fs = file.getFileSystem(conf);
           FSDataOutputStream fileOut = fs.create(file, true);
           if (!isCompressed) 
           {
               return new LineRecordWriter<K, V>(fileOut, keyValueSeparator);
           } 
           else 
           {
               return new LineRecordWriter<K, V>(new DataOutputStream(codec.createOutputStream(fileOut)),keyValueSeparator);
           }
       }
}
